package edu.usc.pocketfarm.pocketfarm

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {
    val app: PocketFarmApplication
        get() {
            return PocketFarmApplication.instance
        }
    fun goToPage(clz: Class<out Activity>, extras: Bundle? = null) {
        val intent = Intent(this, clz)
        //if there are extras, it will be placed here
        if (extras != null) {
            intent.putExtras(extras)
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        //perform the intent
        startActivity(intent)
    }
}