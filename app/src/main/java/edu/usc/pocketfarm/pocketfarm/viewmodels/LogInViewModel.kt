package edu.usc.pocketfarm.pocketfarm.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.Bindable
import edu.usc.pocketfarm.pocketfarm.extensions.isValidEmail

class LogInViewModel : ViewModel() {

    @Bindable
    val email = MutableLiveData<String>()

    @Bindable
    val password = MutableLiveData<String>()

    fun isValidLogIn(): Boolean {
        val email = email.value
        if (email == null || email.isEmpty() || !email.isValidEmail()) {
            return false
        }


        val password = password.value
        if (password == null || password.isEmpty()) {
            return false
        }

        return true
    }
}