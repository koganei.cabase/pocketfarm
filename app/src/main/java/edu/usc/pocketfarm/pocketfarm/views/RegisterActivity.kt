package edu.usc.pocketfarm.pocketfarm.views


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import edu.usc.pocketfarm.pocketfarm.BaseActivity
import edu.usc.pocketfarm.pocketfarm.R
import edu.usc.pocketfarm.pocketfarm.databinding.ActivityRegisterBinding
import edu.usc.pocketfarm.pocketfarm.viewmodels.RegisterViewModel
import kotlinx.android.synthetic.main.activity_register.*
import android.support.v7.app.AlertDialog
import android.view.View
import edu.usc.pocketfarm.pocketfarm.models.User




class RegisterActivity : BaseActivity() {

    private lateinit var mRegisterVm : RegisterViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        mRegisterVm = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        DataBindingUtil
            .setContentView<ActivityRegisterBinding>(this, R.layout.activity_register)
            .apply {
                this.lifecycleOwner = this@RegisterActivity
                this.vm = mRegisterVm
            }
        regLogInTxt.setOnClickListener {
            goToPage(LogInActivity::class.java)
        }
        regSignUpBtn.setOnClickListener {
            if(mRegisterVm.checkInput()){
                app.auth.createUserWithEmailAndPassword(mRegisterVm.email.value!!,mRegisterVm.password.value!!)
                    .addOnSuccessListener {
                        val user = User(
                            it.user.uid,
                            "${mRegisterVm.email.value}",
                            mRegisterVm.firstName.value!!.capitalize(),
                            mRegisterVm.lastName.value!!.capitalize(),
                            mRegisterVm.address.value!!.capitalize()
                        )
                        //todo shaun try to control and add progress bar here please.
                        app.database.collection("Users").document( it.user.uid)
                            .set(user)
                            .addOnSuccessListener {
                                app.auth.currentUser?.sendEmailVerification()
                                    ?.addOnSuccessListener {
                                        val alertDialog = AlertDialog.Builder(this).create()
                                        alertDialog.setTitle("Alert Dialog")
                                        alertDialog.setMessage("A confirmation email has been sent to your email. please tap the button below to check your email")
                                        alertDialog.setButton(
                                            AlertDialog.BUTTON_POSITIVE, "OK"
                                        ) { _, _ ->
                                            val intent = packageManager.getLaunchIntentForPackage("com.google.android.gm")
                                            startActivity(intent)
                                        }
                                        alertDialog.show()
                                    }
                                    ?.addOnFailureListener {
                                        val builder = AlertDialog.Builder(this@RegisterActivity)
                                        builder.setTitle("An error has occured.")
                                        builder.setMessage("The application has failed to send the confirmation email. Check your connection and try again.")
                                        builder.setNeutralButton("OK"){_,_ ->
                                        }
                                        val dialog: AlertDialog = builder.create()
                                        dialog.show()
                                    }
                            }
                            .addOnFailureListener {
                                val builder = AlertDialog.Builder(this@RegisterActivity)
                                builder.setTitle("An error has occured.")
                                builder.setMessage("Error sending to database. Check your connection and try again.")
                                builder.setNeutralButton("OK"){_,_ ->
                                }
                                val dialog: AlertDialog = builder.create()
                                dialog.show()
                            }

                    }
                    .addOnFailureListener {
                        val builder = AlertDialog.Builder(this@RegisterActivity)
                        builder.setTitle("An error has occured.")
                        builder.setMessage("Account creation failure. Check your connection and try again.")
                        builder.setNeutralButton("OK"){_,_ ->
                        }
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    }
            }
            if(!mRegisterVm.isValidName()){
                //your first name or last name is incorrect message
                errFirstName.visibility = View.VISIBLE
                errLastName.visibility = View.VISIBLE
            }
            else{
                errFirstName.visibility = View.GONE
                errLastName.visibility = View.GONE
            }
            if(!mRegisterVm.isValidEmail()){
                //your email is incorrect here
                errEmail.visibility = View.VISIBLE
            }
            else{
                errEmail.visibility = View.GONE
            }
            if(!mRegisterVm.isValidPassword()){
                //password must be longer than 8 characters and must not contain spaces.
                errPwd.visibility = View.VISIBLE
            }
            else{
                errPwd.visibility = View.GONE
            }
            //todo ADDITIONAL shaun, add error message for address(not email)
        }
    }
}
