package edu.usc.pocketfarm.pocketfarm.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.Bindable
import edu.usc.pocketfarm.pocketfarm.extensions.isValidEmail
import java.util.regex.Pattern


class RegisterViewModel : ViewModel() {

    @Bindable
    val email = MutableLiveData<String>()
    @Bindable
    val password = MutableLiveData<String>()
    @Bindable
    val firstName = MutableLiveData<String>()
    @Bindable
    val lastName = MutableLiveData<String>()
    @Bindable
    val address = MutableLiveData<String>()

    fun isValidPassword(): Boolean {
        val isValid = true
        val password = password.value
        if (password.isNullOrBlank()) {
            return false
        }
        if (password.length < 6) {
            return false
        }
        return isValid
    }

    fun isValidEmail(): Boolean {
        val isValid = true
        val email = email.value
        if (email.isNullOrBlank()) {
            return false
        }
        if (!email.isValidEmail()) {
            return false
        }
        return isValid
    }

    fun isValidName(): Boolean {
        val isValid = true
        val firstName = firstName.value
        val lastName = lastName.value
        val pattern = Pattern.compile("^([a-zA-Z]+?)([-\\s'][a-zA-Z]+)*?\$")
        val firstNameMatcher = pattern.matcher(firstName)
        val lastNameMatcher = pattern.matcher(lastName)
        if (firstName.isNullOrBlank()) {
            return false
        }
        if (lastName.isNullOrBlank()) {
            return false
        }
        if (!firstNameMatcher.matches()) {
            return false

        }
        if (!lastNameMatcher.matches()) {
            return false
        }
        return isValid
    }

    fun isValidAddress(): Boolean{
        val isValid = true
        val address =  address.value ?: return false
        return isValid
    }


    fun checkInput(): Boolean {
        val isValid = true
        if (!isValidEmail()) {
            return false
        }
        if (!isValidName()) {
            return false
        }
        if (!isValidPassword()) {
            return false
        }
        if (!isValidAddress()){
            return false
        }
        return isValid
    }
}