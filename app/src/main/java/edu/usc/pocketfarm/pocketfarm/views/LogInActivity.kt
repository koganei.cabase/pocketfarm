package edu.usc.pocketfarm.pocketfarm.views

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Toast
import edu.usc.pocketfarm.pocketfarm.BaseActivity
import edu.usc.pocketfarm.pocketfarm.R
import edu.usc.pocketfarm.pocketfarm.databinding.ActivityLoginBinding
import edu.usc.pocketfarm.pocketfarm.viewmodels.LogInViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LogInActivity : BaseActivity() {
    private lateinit var mLogInVm : LogInViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mLogInVm = ViewModelProviders.of(this).get(LogInViewModel::class.java)
        DataBindingUtil
            .setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
            .apply {
                this.lifecycleOwner = this@LogInActivity
                this.vm = mLogInVm
            }
        lICreateAccTxt.setOnClickListener {
            goToPage(RegisterActivity::class.java)
        }
    }

    fun logInClick(view : View){
        if(mLogInVm.isValidLogIn()){
            signInWithEmailAndPassword("${mLogInVm.email.value}", "${mLogInVm.password.value}")
            login_credErrorTxt.visibility = View.GONE
        }
        else{
            login_credErrorTxt.visibility = View.VISIBLE
        }
    }

    private fun signInWithEmailAndPassword(email: String, password: String) {
        this.app.auth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                if(it.user.isEmailVerified){
                    //go to landing page
                    //todo israel check if this works
                }
                else{
                    login_credErrorTxt.visibility = View.VISIBLE
                    login_credErrorTxt.text = "Email not verified. Please check or try logging in again."
                }
            }
            .addOnFailureListener {
                val builder = AlertDialog.Builder(this@LogInActivity)
                builder.setTitle("Failed to log in.")
                builder.setMessage("The application has failed to log in. Check your connection or try logging in again.")
                builder.setNeutralButton("OK"){_,_ ->
                    Toast.makeText(applicationContext,"Try logging in again",Toast.LENGTH_SHORT).show()
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
    }
}
