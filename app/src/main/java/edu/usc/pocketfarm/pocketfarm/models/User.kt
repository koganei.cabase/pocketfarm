package edu.usc.pocketfarm.pocketfarm.models


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val id: String = "",
    val email: String = "",
    val firstName: String = "",
    val lastName: String = "",
    val deliveryAddress: String = ""
) : Parcelable
