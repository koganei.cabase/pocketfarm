package edu.usc.pocketfarm.pocketfarm.extensions

import java.util.regex.Pattern



private val EMAIL_ADDRESS_REGEX: Pattern = Pattern.compile(
    """^(([\w-]+\.)+[\w-]+|([a-zA-Z]|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$"""
)
//check if email is valid.
fun String.isValidEmail(): Boolean {
    //emailMatches will return a boolean if it passed EMAIL_ADDRESS_REGEX
    val emailMatches = EMAIL_ADDRESS_REGEX.matcher(this).matches()
    return this.isNotEmpty() && emailMatches
}
fun String.capitalizeAndTrim() : String{
    val original = this
    val afterString = original.trim()
    afterString.toLowerCase()
    afterString.capitalize()
    return afterString
}

