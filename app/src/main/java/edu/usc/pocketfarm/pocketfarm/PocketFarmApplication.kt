package edu.usc.pocketfarm.pocketfarm

import android.app.Application
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


class PocketFarmApplication : Application(){
    companion object {
        lateinit var instance: PocketFarmApplication
            private set
    }
    private lateinit var mAuth: FirebaseAuth
    private lateinit var mDatabase: FirebaseFirestore
    val auth : FirebaseAuth
    get(){
        return mAuth
    }
    val database: FirebaseFirestore
    get(){
        return mDatabase
    }
    override fun onCreate() {
        super.onCreate()
        instance = this
        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseFirestore.getInstance()
    }
}